var gulp = require('gulp');
var sass = require('gulp-sass');
var watchSass = require("gulp-watch-sass");
var sourcemaps = require('gulp-sourcemaps');

var globscss = './src/**/*.scss';
var distcss = './css';

gulp.task('sass', function () {
    return gulp.src(globscss)
        .pipe(sass({
            includePaths: ['node_modules/'],
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(gulp.dest(distcss));
});

gulp.task('sass:dev', function () {
    gulp.src(globscss)
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: ['node_modules/'],
        }).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(distcss));
});

gulp.task('sass:watch', function () {
    watchSass([globscss])
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: ['node_modules/'],
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(distcss));
});

// gulp.task('default', ['sass:watch']);
gulp.task('default', gulp.parallel('sass'));